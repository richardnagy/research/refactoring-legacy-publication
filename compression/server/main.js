const express = require('express')
const crypto = require('crypto')
const zlib = require('zlib')
const fs = require('fs')

let port = null
if (process.argv < 3) {
    console.log('No port specified, defaulting to 80')
    port = 80
}
else {
    port = parseInt(process.argv[2])
}

const app = express()
app.use('/client', express.static('client'))

console.log('Compressing...')
const r4kbmp = crypto.randomBytes(3840 * 2160 * 3)
const r2kbmp = crypto.randomBytes(2048 * 1080 * 3)
const images = {
    apppage: fs.readFileSync('./server/images/app-page.png')
}
const compressed = {
    r2kdeflate: zlib.deflateSync(r2kbmp),
    r2kgzip:    zlib.gzipSync(r2kbmp, { level:6 }),
    r2kbrotli:  zlib.brotliCompressSync(r2kbmp),

    imgdeflate: zlib.deflateSync(images.apppage),
    imggzip:    zlib.gzipSync(images.apppage, { level:6 }),
    imgbrotli:  zlib.brotliCompressSync(images.apppage)
}


app.get('/api/image2k', (req, res) => {
    switch (req.query.compress) {
        case 'deflate':
            res.setHeader('Content-Encoding', 'deflate')
            res.send(compressed.imgdeflate)
            break
        case 'gzip':
            res.setHeader('Content-Encoding', 'gzip')
            res.send(compressed.imggzip)
            break
        case 'brotli':
            res.setHeader('Content-Encoding', 'br')
            res.send(compressed.imgbrotli)
            break
        default:
            res.send(images.apppage)
            break
    }
})

app.get('/api/random2k', (req, res) => {
    switch (req.query.compress) {
        case 'deflate':
            res.setHeader('Content-Encoding', 'deflate')
            res.send(compressed.r2kdeflate)
            break
        case 'gzip':
            res.setHeader('Content-Encoding', 'gzip')
            res.send(compressed.r2kgzip)
            break
        case 'brotli':
            res.setHeader('Content-Encoding', 'br')
            res.send(compressed.r2kbrotli)
            break
        default:
            res.send(r2kbmp)
            break
    }
})



app.listen(port, () => {
    console.log('Listening on', port)
})

