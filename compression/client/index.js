const r4kbmpUrl = '/api/image2k'

function median(arr) {
    let i = arr.length / 2
    if (arr.length % 2 == 0) {
        return (arr[ Math.floor(i) ] + arr[ Math.ceil(i) ]) / 2
    }
    else {
        return arr[Math.ceil(i)]
    }
}

async function request(postifx='') {
    const start = performance.now()

    const res = await fetch(r4kbmpUrl + postifx)
    const bytes = await res.arrayBuffer()

    const end = performance.now()
    return end - start
}

async function runTest(name, postfix) {
    console.log(name)
    let results = []
    for (let i = 0; i < 200; i++) {
        results.push(await request(postfix))
    }

    
    console.log("   min: ", Math.min(...results))
    console.log("   max: ", Math.max(...results))
    console.log("   avg: ", results.reduce((a,b) => a + b, 0) / results.length)
    console.log("   med: ", median(results))
}

async function runTests() {
    await runTest('Standard', '')
    await runTest('Compression: deflate', '?compress=deflate')
    await runTest('Compression: gzip', '?compress=gzip')
    await runTest('Compression: brotli', '?compress=br')
}

runTests()