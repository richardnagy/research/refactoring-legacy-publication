## Compression Benchmarks

### 1. Move to project folder

```bash
cd compression
```

### 2. Install dependencies using yarn

> ℹ️ If yarn is not installed: `npm i -g yarn`

```bash
yarn install
```

### 3. Start server

Production mode:

```bash
yarn start
```

Development mode:

```bash
yarn dev
```

### 4. Navigate to page

http://localhost:8080/client/

### 5. Open Developer Console

Key Combination: `Ctrl` + `Shift` + `i`

Then navigate to the `Console` section. Here you will see the results of the compression timings.