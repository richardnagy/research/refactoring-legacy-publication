import json
import uvicorn
from fastapi import FastAPI
from fastapi import Request
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware

PORT = 8000

# Setup app
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# Read data json
with open('./data/image.json', 'r') as file:
    data = json.loads(file.read())


# Fetch request
@app.get("/poll")
def get_request(request: Request):
    return data


if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=PORT, reload=False)